package com.zuleicos.feagurth.peliculasandroid;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class AddArtist extends AppCompatActivity {

    private final String NAMESPACE = "http://sergiodaw2.16mb.com/";
    private final String URL = "http://sergiodaw2.16mb.com/servicio.php?wsdl";
    private final String SOAP_ACTION_PREFIX = "/";
    private final String METHOD_NAME_1 = "altaArtistas";

    private int resultado;
    Button btnAddArtist;
    EditText txtNombreArtista;
    EditText txtApellidoArtista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_artist);

        btnAddArtist = (Button) findViewById(R.id.btnAddArtist);
        txtNombreArtista = (EditText) findViewById(R.id.txtNombre);
        txtApellidoArtista = (EditText) findViewById(R.id.txtApellidos);

        btnAddArtist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nombre = txtNombreArtista.getText().toString();
                String apellidos = txtApellidoArtista.getText().toString();

                if (!nombre.matches("") && !apellidos.matches("")) {
                    AsyncCallWS task = new AsyncCallWS();

                    task.execute();
                } else {
                    Toast.makeText(getApplicationContext(), getApplicationContext().getText(R.string.add_artists_alert), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private class AsyncCallWS extends AsyncTask<String, Void, Void> {

        private String TAG = "Depuración:";

        @Override
        protected Void doInBackground(String... params) {
            Log.i(TAG, "doInBackground");
            altaArtista();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i(TAG, "onPostExecute");

            if(resultado == 0)
            {
                Toast.makeText(getApplicationContext(), getApplicationContext().getText(R.string.add_artists_ok), Toast.LENGTH_LONG).show();
                txtNombreArtista.setText("");
                txtApellidoArtista.setText("");
            }
            else
            {
                Toast.makeText(getApplicationContext(), getApplicationContext().getText(R.string.add_artists_error), Toast.LENGTH_LONG).show();
            }

        }

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            Log.i(TAG, "onProgressUpdate");
        }

    }


    public void altaArtista() {
        try {
            // SoapEnvelop.VER11 is SOAP Version 1.1 constant
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);


            resultado = peticiones(envelope, METHOD_NAME_1, new String[]{"nombreArtista", "apellidosArtista"}, new String[]{txtNombreArtista.getText().toString(), txtApellidoArtista.getText().toString()});

        } catch (Exception e) {
            e.printStackTrace();
            resultado = -1;

        }
    }

    public int peticiones(SoapSerializationEnvelope envelope, String metodo, String[] propiedad, String[] valor) {

        int salida = -1;

        //Create request
        SoapObject request = new SoapObject(NAMESPACE, metodo);


        if (propiedad.length > 0 && valor.length > 0) {

            for (int i = 0; i < propiedad.length; i++) {
                request.addProperty(propiedad[i], valor[i]);
            }

        }

        envelope.bodyOut = request;

        HttpTransportSE transport = new HttpTransportSE(URL);

        try {
            transport.call(NAMESPACE + SOAP_ACTION_PREFIX + metodo, envelope);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (envelope.bodyIn != null) {


            salida = Integer.valueOf(((SoapObject) envelope.bodyIn).getProperty(0).toString());


        } else {
            salida = -1;
        }
        return salida;
    }
}
