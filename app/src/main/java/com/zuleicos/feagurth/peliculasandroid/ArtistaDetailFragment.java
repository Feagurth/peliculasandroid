package com.zuleicos.feagurth.peliculasandroid;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zuleicos.feagurth.peliculasandroid.POJO.Artista;


/**
 * A fragment representing a single Artista detail screen.
 * This fragment is either contained in a {@link ArtistaListActivity}
 * in two-pane mode (on tablets) or a {@link ArtistaDetailActivity}
 * on handsets.
 */
public class ArtistaDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "id_artista";

    private Artista mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArtistaDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            for (Artista d : ArtistaListActivity.artistas) {
                if (d.getId_artista() == Integer.valueOf(getArguments().getString(ARG_ITEM_ID))) {
                    mItem = d;
                    break;
                }
            }



            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                //appBarLayout.setTitle(mItem.content);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.artista_detail, container, false);


        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.artista_detail)).setText(mItem.getNombreCompleto());
        }

        return rootView;
    }
}
