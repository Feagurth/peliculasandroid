package com.zuleicos.feagurth.peliculasandroid;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zuleicos.feagurth.peliculasandroid.POJO.Artista;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of Artistas. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ArtistaDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ArtistaListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    public static List<Artista> artistas = new ArrayList<>();

    private final String NAMESPACE = "http://sergiodaw2.16mb.com/";
    private final String URL = "http://sergiodaw2.16mb.com/servicio.php?wsdl";
    private final String SOAP_ACTION_PREFIX = "/";
    private final String METHOD_NAME_1 = "listaArtistas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artista_list);

        AsyncCallWS task = new AsyncCallWS();

        //Call execute
        task.execute();

       // View recyclerView = findViewById(R.id.artista_list);
        //assert recyclerView != null;
        //setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.artista_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(artistas));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<Artista> mValues;

        public SimpleItemRecyclerViewAdapter(List<Artista> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.artista_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(String.valueOf(mValues.get(position).getId_artista()));
            holder.mContentView.setText(mValues.get(position).getNombreCompleto() );

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(ArtistaDetailFragment.ARG_ITEM_ID, String.valueOf(holder.mItem.getId_artista()));
                        ArtistaDetailFragment fragment = new ArtistaDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.artista_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, ArtistaDetailActivity.class);
                        intent.putExtra(ArtistaDetailFragment.ARG_ITEM_ID, String.valueOf(holder.mItem.getId_artista()));

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public Artista mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }

    private class AsyncCallWS extends AsyncTask<String, Void, Void> {

        private String TAG = "Depuración:";

        @Override
        protected Void doInBackground(String... params) {
            Log.i(TAG, "doInBackground");
            listarArtistas();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i(TAG, "onPostExecute");

            View recyclerView = findViewById(R.id.artista_list);
            assert recyclerView != null;
            setupRecyclerView((RecyclerView) recyclerView);

        }

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            Log.i(TAG, "onProgressUpdate");
        }

    }


    public void listarArtistas() {
        try {
            // SoapEnvelop.VER11 is SOAP Version 1.1 constant
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);

            artistas = peticiones(envelope, METHOD_NAME_1, "", "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Artista> peticiones(SoapSerializationEnvelope envelope, String metodo, String propiedad, String valor) {
        List<Artista> lista;

        //Create request
        SoapObject request = new SoapObject(NAMESPACE, metodo);


        if (propiedad != "" && valor != "") {
            //bodyOut is the body object to be sent out with this envelope
            //Add the property to request object
            request.addProperty(propiedad, valor);
        }

        envelope.bodyOut = request;

        HttpTransportSE transport = new HttpTransportSE(URL);

        try {
            transport.call(NAMESPACE + SOAP_ACTION_PREFIX + metodo, envelope);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (envelope.bodyIn != null) {

            // Creamos la lista haciendo uso de la función SOAP2Peliculas
            lista = SOAP2Artistas(((SoapObject) envelope.bodyIn));

        } else {
            lista = null;
        }
        return lista;
    }


    /**
     * Función que nos permite convertir el objeto SoapObject de la respuesta a una lista de objetos Pelicula
     *
     * @param cuerpo El objeto SoapObject correspondiente a la propiedad bodyIn de la envoltura de la respuesa SOAP
     * @return Una lista de objetos Pelicula con la información de las peliculas
     */
    private List<Artista> SOAP2Artistas(SoapObject cuerpo) {

        // Creamos una lista de peliculas para devolver
        List<Artista> listaArtistas = new ArrayList<>();

        // Recuperamos el array que contiene la información de las películas
        SoapObject array = (SoapObject) cuerpo.getProperty(0);

        // Iteramos por el array
        for (int i = 0; i < array.getPropertyCount(); i++) {

            // Recuperamos la propiedad que corresponde a la pelicula de la iteración
            SoapObject objArti = (SoapObject) array.getProperty(i);

            // Creamos un objeto Pelicula
            Artista arti = new Artista();

            // Recuperamos los datos generales de la película haciendo uso de las propiedades
            int id_artista = Integer.valueOf(objArti.getProperty(0).toString());
            String nombre = objArti.getProperty(1).toString();
            String apellidos = objArti.getProperty(2).toString();




            // Asignamos al objeto Pelicula la información recuperada
            arti.setId_artista(id_artista);
            arti.setNombre(nombre);
            arti.setApellidos(apellidos);

            // Añadimos la película generada a la lista correspondiente
            listaArtistas.add(arti);
        }

        // Devolvemos el resultado
        return listaArtistas;

    }
}
