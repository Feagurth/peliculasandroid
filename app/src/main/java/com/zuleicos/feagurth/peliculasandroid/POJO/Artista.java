package com.zuleicos.feagurth.peliculasandroid.POJO;

/**
 * Created by Super on 11/03/2016.
 */
public class Artista {

    private int id_artista;
    private String nombre;
    private String apellidos;

    public Artista(int id_artista, String nombre, String apellidos) {
        this.id_artista = id_artista;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }

    public Artista() {
    }

    public int getId_artista() {
        return id_artista;
    }

    public void setId_artista(int id_artista) {
        this.id_artista = id_artista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreCompleto()
    {
        return this.nombre + " " + this.apellidos;
    }
}
