package com.zuleicos.feagurth.peliculasandroid.POJO;

import java.util.List;

/**
 * Created by Super on 11/03/2016.
 */
public class Pelicula {

    private int id_pelicula;
    private String nombre;
    private String anyo;
    private Genero genero;
    private String sinopsis;
    private int duracion;
    private List<Artista> actores;
    private List<Artista> directores;
    private List<Artista> guionistas;

    public Pelicula() {
    }

    public int getId_pelicula() {
        return id_pelicula;
    }

    public void setId_pelicula(int id_pelicula) {
        this.id_pelicula = id_pelicula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAnyo() {
        return anyo;
    }

    public void setAnyo(String anyo) {
        this.anyo = anyo;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public List<Artista> getActores() {
        return actores;
    }

    public void setActores(List<Artista> actores) {
        this.actores = actores;
    }

    public List<Artista> getDirectores() {
        return directores;
    }

    public void setDirectores(List<Artista> directores) {
        this.directores = directores;
    }

    public List<Artista> getGuionistas() {
        return guionistas;
    }

    public void setGuionistas(List<Artista> guionistas) {
        this.guionistas = guionistas;
    }

    public String getInfoCompleta()
    {
        String datosPelicula = "";

        datosPelicula += "Año: " + this.anyo + "\r\n";
        datosPelicula += "Genero: " + this.genero.getTipo() + "\r\n";
        datosPelicula += "Duración: " + this.duracion + "\r\n";

        datosPelicula += "\r\n";

        datosPelicula += "Director/es: ";

        for (Artista artista : this.directores) {

            datosPelicula += artista.getNombreCompleto() + ", ";
        }

        datosPelicula = datosPelicula.substring(0, datosPelicula.length() - 2);

        datosPelicula += "\r\n";

        datosPelicula += "Guionista/s: ";

        for (Artista artista : this.guionistas) {

            datosPelicula += artista.getNombreCompleto() + ", ";
        }

        datosPelicula = datosPelicula.substring(0, datosPelicula.length() - 2);

        datosPelicula += "\r\n";

        datosPelicula += "Interprete/s: ";

        for (Artista artista : this.actores) {

            datosPelicula += artista.getNombreCompleto() + ", ";
        }

        datosPelicula = datosPelicula.substring(0, datosPelicula.length() - 2);

        datosPelicula += "\r\n";
        datosPelicula += "\r\n";

        datosPelicula += "Sinopsis" + "\r\n";

        datosPelicula += this.sinopsis;

        return datosPelicula;
    }
}