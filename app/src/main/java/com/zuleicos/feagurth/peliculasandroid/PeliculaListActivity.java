package com.zuleicos.feagurth.peliculasandroid;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zuleicos.feagurth.peliculasandroid.POJO.Artista;
import com.zuleicos.feagurth.peliculasandroid.POJO.Genero;
import com.zuleicos.feagurth.peliculasandroid.POJO.Pelicula;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;


/**
 * An activity representing a list of Peliculas. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PeliculaDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class PeliculaListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    public static List<Pelicula> peliculas = new ArrayList<>();

    private final String NAMESPACE = "http://sergiodaw2.16mb.com/";
    private final String URL = "http://sergiodaw2.16mb.com/servicio.php?wsdl";
    private final String SOAP_ACTION_PREFIX = "/";
    private final String METHOD_NAME_1 = "listaPeliculas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelicula_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        AsyncCallWS task = new AsyncCallWS();

        //Call execute
        task.execute();


        //View recyclerView = findViewById(R.id.pelicula_list);
        //assert recyclerView != null;
        //setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.pelicula_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(peliculas));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<Pelicula> mValues;

        public SimpleItemRecyclerViewAdapter(List<Pelicula> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.pelicula_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(String.valueOf(mValues.get(position).getId_pelicula()));
            holder.mContentView.setText(mValues.get(position).getNombre());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(PeliculaDetailFragment.ARG_ITEM_ID, String.valueOf(holder.mItem.getId_pelicula()));
                        PeliculaDetailFragment fragment = new PeliculaDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.pelicula_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, PeliculaDetailActivity.class);
                        intent.putExtra(PeliculaDetailFragment.ARG_ITEM_ID, String.valueOf(holder.mItem.getId_pelicula()));

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public Pelicula mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }

    private class AsyncCallWS extends AsyncTask<String, Void, Void> {

        private String TAG = "Depuración:";

        @Override
        protected Void doInBackground(String... params) {
            Log.i(TAG, "doInBackground");
            listarPeliculas();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i(TAG, "onPostExecute");

            View recyclerView = findViewById(R.id.pelicula_list);
            assert recyclerView != null;
            setupRecyclerView((RecyclerView) recyclerView);

        }

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute");
        /*
        tv.setText(String.format("Obteniendo...%d", numero_receta));
        */
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            Log.i(TAG, "onProgressUpdate");
        }

    }


    public void listarPeliculas() {
        try {
            // SoapEnvelop.VER11 is SOAP Version 1.1 constant
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);

            peliculas = peticiones(envelope, METHOD_NAME_1, "", "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Pelicula> peticiones(SoapSerializationEnvelope envelope, String metodo, String propiedad, String valor) {
        List<Pelicula> lista;

        //Create request
        SoapObject request = new SoapObject(NAMESPACE, metodo);


        if (propiedad != "" && valor != "") {
            //bodyOut is the body object to be sent out with this envelope
            //Add the property to request object
            request.addProperty(propiedad, valor);
        }

        envelope.bodyOut = request;

        HttpTransportSE transport = new HttpTransportSE(URL);

        try {
            transport.call(NAMESPACE + SOAP_ACTION_PREFIX + metodo, envelope);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (envelope.bodyIn != null) {

            // Creamos la lista haciendo uso de la función SOAP2Peliculas
            lista = SOAP2Peliculas(((SoapObject) envelope.bodyIn));

        } else {
            lista = null;
        }
        return lista;
    }


    /**
     * Función que nos permite convertir el objeto SoapObject de la respuesta a una lista de objetos Pelicula
     *
     * @param cuerpo El objeto SoapObject correspondiente a la propiedad bodyIn de la envoltura de la respuesa SOAP
     * @return Una lista de objetos Pelicula con la información de las peliculas
     */
    private List<Pelicula> SOAP2Peliculas(SoapObject cuerpo) {

        // Creamos una lista de peliculas para devolver
        List<Pelicula> listaPeliculas = new ArrayList<>();

        // Recuperamos el array que contiene la información de las películas
        SoapObject array = (SoapObject) cuerpo.getProperty(0);

        // Iteramos por el array
        for (int i = 0; i < array.getPropertyCount(); i++) {

            // Recuperamos la propiedad que corresponde a la pelicula de la iteración
            SoapObject objPeli = (SoapObject) array.getProperty(i);

            // Creamos un objeto Pelicula
            Pelicula peli = new Pelicula();

            // Recuperamos los datos generales de la película haciendo uso de las propiedades
            int id_pelicula = Integer.valueOf(objPeli.getProperty(0).toString());
            String nombre = objPeli.getProperty(1).toString();
            String anyo = objPeli.getProperty(2).toString();
            String sinopsis = objPeli.getProperty(4).toString();
            int duracion = Integer.valueOf(objPeli.getProperty(5).toString());

            // Usamos la función SOAP2Genero para recuperar la información del genero de la
            // película y crear un objeto Genero con ella
            Genero genero = SOAP2Genero((SoapObject) objPeli.getProperty(3));

            // Creamos 3 listas de Artistas para almacenar le información de directors, guionistas
            // y actores
            List<Artista> directores;
            List<Artista> guionistas;
            List<Artista> actores;

            // Usamos la función SOAP2Artista para recuperar la información y crear con ella listas
            // de objetos Artista con la información de los mismos
            directores = SOAP2Artista((SoapObject) objPeli.getProperty(6));
            guionistas = SOAP2Artista((SoapObject) objPeli.getProperty(7));
            actores = SOAP2Artista((SoapObject) objPeli.getProperty(8));


            // Asignamos al objeto Pelicula la información recuperada
            peli.setId_pelicula(id_pelicula);
            peli.setNombre(nombre);
            peli.setAnyo(anyo);
            peli.setDuracion(duracion);
            peli.setSinopsis(sinopsis);
            peli.setGenero(genero);

            peli.setDirectores(directores);
            peli.setGuionistas(guionistas);
            peli.setActores(actores);

            // Añadimos la película generada a la lista correspondiente
            listaPeliculas.add(peli);
        }

        // Devolvemos el resultado
        return listaPeliculas;

    }


    /**
     * Función que nos permite convertir el objeto SoapObject de la respuesta a un objeto Genero
     *
     * @param valor El objeto SoapObject que contiene la información del genero
     * @return Un objeto Genero
     */
    private Genero SOAP2Genero(SoapObject valor) {

        int id_genero = Integer.valueOf(valor.getProperty(0).toString());
        String tipo = valor.getProperty(1).toString();

        return new Genero(id_genero, tipo);

    }


    /**
     * Función que nos permite convertir el objeto SoapObject de la respuesta a una lista de Artistas
     *
     * @param valor El objeto SoapObject que contiene la información de los artistas
     * @return Una lista de objetos Artista
     */
    private List<Artista> SOAP2Artista(SoapObject valor) {

        // Creamos una lista de artistas para devolver
        List<Artista> artistas = new ArrayList<>();

        // Iteramos por todas la propiedades del objeto
        for (int j = 0; j < valor.getPropertyCount(); j++) {

            // Convertimos el objeto de la iteración en un objeto SoapObject
            SoapObject objArtista = ((SoapObject) valor.getProperty(j));

            // Recuperamos los valores de las propiedades del objeto
            int id_artista = Integer.valueOf(objArtista.getProperty(0).toString());
            String nombreArtista = objArtista.getProperty(1).toString();
            String apellidoArtista = objArtista.getProperty(2).toString();

            // Creamos un nuevo objeto Artista y lo añadimos a la lista
            artistas.add(new Artista(id_artista, nombreArtista, apellidoArtista));
        }

        // Devolvemos la lista
        return artistas;

    }
}
